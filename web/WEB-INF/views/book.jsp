<%-- 
    Document   : book
    Created on : Aug 26, 2023, 10:45:26 AM
    Author     : Anton Andreev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Подробная информация о книге</title>
    <link rel="stylesheet" href="css/styles.css"> 
</head>
<body>

<div class="container">
    <h1>Подробная информация о книге</h1>

    <div class="book-details">
        <img src="path_to_book_cover.jpg" alt="Обложка книги">
        <h2>Название книги</h2>
        <p><strong>Автор:</strong> Имя Автора</p>
        <p><strong>Издатель:</strong> Издательство</p>
        <p><strong>Год издания:</strong> 2023</p>
        <!-- Дополнительные данные о книге могут быть добавлены здесь -->
    </div>

    <a href="read.jsp" class="back-link">Назад к списку книг</a>
</div>

</body>
</html>
