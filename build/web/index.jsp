<%-- 
    Document   : index
    Created on : Aug 26, 2023, 10:41:01 AM
    Author     : Anton Andreev
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Детская Библиотека</title>
    <link rel="stylesheet" href="css/styles.css"> 
</head>
<body>

<div class="container">
    <h1>Добро пожаловать в детскую библиотеку!</h1>
    <p>Выберите, что вы хотите сделать:</p>

    <ul>
        <li><a href="read.jsp">Просмотреть книги</a></li>
        <li><a href="add.jsp">Добавить книгу</a></li>
        <li><a href="edit.jsp">Редактировать книгу</a></li>
    </ul>
</div>

</body>
</html>